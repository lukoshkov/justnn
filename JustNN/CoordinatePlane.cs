﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JustNN
{
	public partial class CoordinatePlane : UserControl
	{
		public List<PointF> PointsA { get; set; }
		public List<PointF> PointsB { get; set; }
		public float XSize { get; set; }
		public float YSize { get; set; }
		public bool GridVisible { get; set; }
		public bool TagsVisible { get; set; }
		public List<PointSet> Sets { get; private set; }

		public CoordinatePlane()
		{
			InitializeComponent();
			DoubleBuffered = true;
			XSize = 10;
			YSize = 10;
			PointsA = new List<PointF>();
			PointsB = new List<PointF>();
			Sets = new List<PointSet>();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			Invalidate();
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			var g = e.Graphics;
			var hw = Width / 2;
			var hh = Height / 2;
			var paddingX = 20;
			var paddingY = 20;
			var xUnit = ((hw - paddingX) / XSize);
			var yUnit = ((hh - paddingY) / YSize);

			var serifSize = 3;
			g.DrawLine(Pens.Black, new Point(0, hh), new Point(Width, hh));
			g.DrawLine(Pens.Black, new Point(hw, 0), new Point(hw, Height));
			if (TagsVisible)
			{
				g.DrawString("0", Font, Brushes.Black, hw + serifSize, hh + serifSize);
			}

			for (int i = 0; i < XSize; i++)
			{
				var j = i + 1;
				var x = j * xUnit;
				var negX = hw - x;
				var posX = hw + x;
				if (GridVisible)
				{
					g.DrawLine(Pens.LightGray, new PointF(negX, paddingY), new PointF(negX, Height - paddingY));
					g.DrawLine(Pens.LightGray, new PointF(posX, paddingY), new PointF(posX, Height - paddingY));
				}
				g.DrawLine(Pens.Black, new PointF(negX, hh - serifSize), new PointF(negX, hh + serifSize));
				g.DrawLine(Pens.Black, new PointF(posX, hh - serifSize), new PointF(posX, hh + serifSize));
				if (TagsVisible)
				{
					g.DrawString((-j).ToString(), Font, Brushes.Black, negX, hh + serifSize);
					g.DrawString(j.ToString(), Font, Brushes.Black, posX, hh + serifSize);
				}
			}
			for (int i = 0; i < YSize; i++)
			{
				var j = i + 1;
				var y = j * yUnit;
				var negY = hh + y;
				var posY = hh - y;
				if (GridVisible)
				{
					g.DrawLine(Pens.LightGray, new PointF(paddingX, negY), new PointF(Width - paddingX, negY));
					g.DrawLine(Pens.LightGray, new PointF(paddingX, posY), new PointF(Width - paddingX, posY));
				}
				g.DrawLine(Pens.Black, new PointF(hw - serifSize, negY), new PointF(hw + serifSize, negY));
				g.DrawLine(Pens.Black, new PointF(hw - serifSize, posY), new PointF(hw + serifSize, posY));
				if (TagsVisible)
				{
					g.DrawString((-j).ToString(), Font, Brushes.Black, hw + serifSize, negY);
					g.DrawString(j.ToString(), Font, Brushes.Black, hw + serifSize, posY);
				}
			}

			DrawPoints(g, PointsA, hw, hh, xUnit, yUnit, Pens.Red);
			DrawPoints(g, PointsB, hw, hh, xUnit, yUnit, Pens.Blue);

			foreach (var set in Sets)
			{
				if (set != null && set.Points != null)
				{
					if (set.Type == PointSet.PointSetType.Points)
					{
						DrawPoints(g, set.Points, hw, hh, xUnit, yUnit, set.Pen);
					}
					else if (set.Type == PointSet.PointSetType.Line)
					{
						DrawLine(g, set.Points, hw, hh, xUnit, yUnit, set.Pen);
					}
				}
			}
		}

		private void DrawPoints(Graphics g, List<PointF> points, int hw, int hh, float xUnit, float yUnit, Pen pen)
		{
			var size = 6;
			if (points != null)
			{
				foreach (var point in points)
				{
					var x = (int)(hw + point.X * xUnit);
					var y = (int)(hh - point.Y * yUnit);
					g.DrawEllipse(pen, x - size / 2, y - size / 2, size, size);
				}
			}
		}

		private void DrawLine(Graphics g, List<PointF> points, int hw, int hh, float xUnit, float yUnit, Pen pen)
		{
			if (points != null)
			{
				for (int i = 0; i < points.Count - 1; i++)
				{
					var x1 = (int)(hw + points[i].X * xUnit);
					var y1 = (int)(hh - points[i].Y * yUnit);
					var x2 = (int)(hw + points[i + 1].X * xUnit);
					var y2 = (int)(hh - points[i + 1].Y * yUnit);
					g.DrawLine(pen, x1, y1, x2, y2);
				}
			}
		}

		private PointF PointToPixelPos(PointF point, int hw, int hh, float xUnit, float yUnit)
		{
			return new PointF(hw + point.X * xUnit, hh - point.Y * yUnit);
		}
	}
}
