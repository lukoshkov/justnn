﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JustNN
{
	static class Program
	{
		/// <summary>
		/// Главная точка входа для приложения.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			//Application.Run(new SettingsForm());
			//Application.Run(new Lab2.DigitForm());
			//Application.Run(new Lab5.DigitForm());
			Application.Run(new Lab6.Form1());
		}
	}
}
