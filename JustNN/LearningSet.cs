﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNN
{
	public class LearningSet
	{
		public List<PointF> Points { get; set; }
		public List<int> Classes { get; set; }

		public LearningSet()
		{
			Points = new List<PointF>();
			Classes = new List<int>();
		}

		public void Init(List<PointF> set1, List<PointF> set2)
		{
			var ind1 = GetIndexList(set1.Count);
			var ind2 = GetIndexList(set1.Count);
			var random = new Random();
			for (int i = 0; i < set1.Count + set2.Count; i++)
			{
				var choice = random.Next(0, 2);
				if (choice == 0 && ind1.Count > 0)
				{
					var index = random.Next(0, ind1.Count);
					Points.Add(set1[ind1[index]]);
					Classes.Add(1);
				}
				else
				{
					var index = random.Next(0, ind2.Count);
					Points.Add(set2[ind2[index]]);
					Classes.Add(-1);
				}
			}
		}

		private static List<int> GetIndexList(int size)
		{
			var indexes = new List<int>(size);
			for (int i = 0; i < size; i++)
			{
				indexes.Add(i);
			}
			return indexes;
		}
	}
}
