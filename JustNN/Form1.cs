﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JustNN
{
	public partial class Form1 : Form
	{
		private Random random;

		public CoordinatePlane Plane { get { return coordinatePlane1; } }

		public Form1()
		{
			InitializeComponent();
			random = new Random();
			ClientSize = new Size(600, 600);
			GeneratePoints();
		}

		public void SetLine(Func<float, float> f, int count)
		{
			var set = new PointSet(Pens.Green, PointSet.PointSetType.Line);
			set.SetPointsByFunc(f, -10, 10, count);
			if (coordinatePlane1.Sets.Count == 0)
			{
				coordinatePlane1.Sets.Add(set);
			}
			else
			{
				coordinatePlane1.Sets[0] = set;
			}
		}

		public void SavePoints(string filename)
		{
			using (var file = new System.IO.StreamWriter(filename))
			{
				foreach (var point in coordinatePlane1.PointsA)
				{
					file.WriteLine("{0} {1} {2}", point.X, point.Y, 1);
				}
				foreach (var point in coordinatePlane1.PointsB)
				{
					file.WriteLine("{0} {1} {2}", point.X, point.Y, 2);
				}
			}
		}

		public void LoadPoints(string filename)
		{
			using (var file = new System.IO.StreamReader(filename))
			{
				coordinatePlane1.PointsA.Clear();
				coordinatePlane1.PointsB.Clear();
				while (!file.EndOfStream)
				{
					try
					{
						var line = file.ReadLine().Split(' ');
						var x = float.Parse(line[0]);
						var y = float.Parse(line[1]);
						var c = int.Parse(line[2]);
						if (c == 1)
						{
							coordinatePlane1.PointsA.Add(new PointF(x, y));
						}
						else if (c == 2)
						{
							coordinatePlane1.PointsB.Add(new PointF(x, y));
						}
					}
					catch (Exception ex)
					{ }
				}
				coordinatePlane1.Invalidate();
			}
		}

		public void GeneratePoints()
		{
			coordinatePlane1.PointsA = GeneratePointsInRectangle(random, new RectangleF(-8, -8, 7, 12), 100);
			//coordinatePlane1.PointsB = GeneratePointsInRectangle(random, new RectangleF(0, 0, 10, 10), 50);
			coordinatePlane1.PointsB = GeneratePointsInCircle(random, new PointF(5, 5), 5, 100);
			//coordinatePlane1.PointsB = GeneratePointsInTriangle(random, new PointF(3, 3), new PointF(6, 5), new PointF(-1, 7), 500);
			coordinatePlane1.Invalidate();
		}

		public static List<PointF> GeneratePointsInRectangle(Random random, RectangleF rect, int count)
		{
			var result = new List<PointF>();
			for (int i = 0; i < count; i++)
			{
				var x = (float)(rect.X + random.NextDouble() * rect.Width);
				var y = (float)(rect.Y + random.NextDouble() * rect.Height);
				result.Add(new PointF(x, y));
			}
			return result;
		}

		public static List<PointF> GeneratePointsInCircle(Random random, PointF pos, float radius, int count)
		{
			var result = new List<PointF>();
			for (int i = 0; i < count; i++)
			{
				var t = 2 * Math.PI * random.NextDouble();
				var u = random.NextDouble() + random.NextDouble();
				var r = u > 1 ? 2 - u : u;
				r *= radius;
				var x = pos.X + (float)(r * Math.Cos(t));
				var y = pos.Y + (float)(r * Math.Sin(t));
				result.Add(new PointF(x, y));
			}
			return result;
		}

		public static List<PointF> GeneratePointsInTriangle(Random random, PointF a, PointF b, PointF c, int count)
		{
			var result = new List<PointF>();

			var v1 = new PointF(b.X - a.X, b.Y - a.Y);
			var v2 = new PointF(c.X - a.X, c.Y - a.Y);
			var v = new PointF(v1.X + v2.X, v1.Y + v2.Y);
			var hv = new PointF(v.X / 2, v.Y / 2);
			var hhv = new PointF(hv.X / 2, hv.Y / 2);
			var bc = new PointF(c.X - b.X, c.Y - b.Y);
			var sign = Cross(new PointF(hhv.X - b.X, hhv.Y - b.Y), bc);
			var vSqr = v.X * v.X + v.Y * v.Y;
			var ccc = 0;
			for (int i = 0; i < count; i++)
			{
				var r1 = random.NextDouble();
				var r2 = random.NextDouble();

				var p = new PointF((float)(r1 * v1.X + r2 * v2.X), (float)(r1 * v1.Y + r2 * v2.Y));
				var pSqr = p.X * p.X + p.Y * p.Y;
				//if (pSqr > vSqr / 4)
				if (Cross(new PointF(p.X - b.X, p.Y - b.Y), bc) < 0)
				{
					var dx = hv.X - p.X;
					var dy = hv.Y - p.Y;
					p.X += 2 * dx;
					p.Y += 2 * dy;
					ccc++;
				}
				p.X += a.X;
				p.Y += a.Y;
				result.Add(p);
			}
			return result;
		}

		private static float Cross(PointF a, PointF b)
		{
			return a.X * b.Y - b.X * a.Y;
		}
	}
}
