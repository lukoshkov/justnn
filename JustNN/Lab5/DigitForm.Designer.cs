﻿namespace JustNN.Lab5
{
	partial class DigitForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.buttonSaveMatrix = new System.Windows.Forms.Button();
			this.buttonLoadMatrix = new System.Windows.Forms.Button();
			this.buttonClearMatrix = new System.Windows.Forms.Button();
			this.buttonRecognize = new System.Windows.Forms.Button();
			this.buttonWTA = new System.Windows.Forms.Button();
			this.buttonSaveWeights = new System.Windows.Forms.Button();
			this.buttonLoadWeights = new System.Windows.Forms.Button();
			this.buttonClearWeights = new System.Windows.Forms.Button();
			this.tbResult = new System.Windows.Forms.TextBox();
			this.listBoxMessages = new System.Windows.Forms.ListBox();
			this.matrixControl1 = new JustNN.Lab2.MatrixControl();
			this.buttonNeuronGas = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new System.Drawing.Point(363, 275);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(119, 153);
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// buttonSaveMatrix
			// 
			this.buttonSaveMatrix.Location = new System.Drawing.Point(326, 42);
			this.buttonSaveMatrix.Name = "buttonSaveMatrix";
			this.buttonSaveMatrix.Size = new System.Drawing.Size(75, 23);
			this.buttonSaveMatrix.TabIndex = 2;
			this.buttonSaveMatrix.Text = "Save matrix";
			this.buttonSaveMatrix.UseVisualStyleBackColor = true;
			this.buttonSaveMatrix.Click += new System.EventHandler(this.buttonSaveMatrix_Click);
			// 
			// buttonLoadMatrix
			// 
			this.buttonLoadMatrix.Location = new System.Drawing.Point(326, 13);
			this.buttonLoadMatrix.Name = "buttonLoadMatrix";
			this.buttonLoadMatrix.Size = new System.Drawing.Size(75, 23);
			this.buttonLoadMatrix.TabIndex = 3;
			this.buttonLoadMatrix.Text = "Load matrix";
			this.buttonLoadMatrix.UseVisualStyleBackColor = true;
			this.buttonLoadMatrix.Click += new System.EventHandler(this.buttonLoadMatrix_Click);
			// 
			// buttonClearMatrix
			// 
			this.buttonClearMatrix.Location = new System.Drawing.Point(326, 71);
			this.buttonClearMatrix.Name = "buttonClearMatrix";
			this.buttonClearMatrix.Size = new System.Drawing.Size(75, 23);
			this.buttonClearMatrix.TabIndex = 4;
			this.buttonClearMatrix.Text = "Clear matrix";
			this.buttonClearMatrix.UseVisualStyleBackColor = true;
			this.buttonClearMatrix.Click += new System.EventHandler(this.buttonClearMatrix_Click);
			// 
			// buttonRecognize
			// 
			this.buttonRecognize.Location = new System.Drawing.Point(326, 119);
			this.buttonRecognize.Name = "buttonRecognize";
			this.buttonRecognize.Size = new System.Drawing.Size(75, 23);
			this.buttonRecognize.TabIndex = 5;
			this.buttonRecognize.Text = "Recognize";
			this.buttonRecognize.UseVisualStyleBackColor = true;
			this.buttonRecognize.Click += new System.EventHandler(this.buttonRecognize_Click);
			// 
			// buttonWTA
			// 
			this.buttonWTA.Location = new System.Drawing.Point(407, 119);
			this.buttonWTA.Name = "buttonWTA";
			this.buttonWTA.Size = new System.Drawing.Size(75, 23);
			this.buttonWTA.TabIndex = 6;
			this.buttonWTA.Text = "WTA";
			this.buttonWTA.UseVisualStyleBackColor = true;
			this.buttonWTA.Click += new System.EventHandler(this.buttonWTA_Click);
			// 
			// buttonSaveWeights
			// 
			this.buttonSaveWeights.Location = new System.Drawing.Point(407, 42);
			this.buttonSaveWeights.Name = "buttonSaveWeights";
			this.buttonSaveWeights.Size = new System.Drawing.Size(75, 23);
			this.buttonSaveWeights.TabIndex = 7;
			this.buttonSaveWeights.Text = "Save weight";
			this.buttonSaveWeights.UseVisualStyleBackColor = true;
			this.buttonSaveWeights.Click += new System.EventHandler(this.buttonSaveWeights_Click);
			// 
			// buttonLoadWeights
			// 
			this.buttonLoadWeights.Location = new System.Drawing.Point(407, 12);
			this.buttonLoadWeights.Name = "buttonLoadWeights";
			this.buttonLoadWeights.Size = new System.Drawing.Size(75, 23);
			this.buttonLoadWeights.TabIndex = 8;
			this.buttonLoadWeights.Text = "Load weight";
			this.buttonLoadWeights.UseVisualStyleBackColor = true;
			this.buttonLoadWeights.Click += new System.EventHandler(this.buttonLoadWeights_Click);
			// 
			// buttonClearWeights
			// 
			this.buttonClearWeights.Location = new System.Drawing.Point(407, 71);
			this.buttonClearWeights.Name = "buttonClearWeights";
			this.buttonClearWeights.Size = new System.Drawing.Size(75, 23);
			this.buttonClearWeights.TabIndex = 9;
			this.buttonClearWeights.Text = "Clear weight";
			this.buttonClearWeights.UseVisualStyleBackColor = true;
			this.buttonClearWeights.Click += new System.EventHandler(this.buttonClearWeights_Click);
			// 
			// tbResult
			// 
			this.tbResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.tbResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbResult.ForeColor = System.Drawing.Color.LightPink;
			this.tbResult.Location = new System.Drawing.Point(326, 149);
			this.tbResult.Name = "tbResult";
			this.tbResult.Size = new System.Drawing.Size(75, 109);
			this.tbResult.TabIndex = 10;
			this.tbResult.Text = "0";
			this.tbResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// listBoxMessages
			// 
			this.listBoxMessages.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listBoxMessages.FormattingEnabled = true;
			this.listBoxMessages.Location = new System.Drawing.Point(13, 337);
			this.listBoxMessages.Name = "listBoxMessages";
			this.listBoxMessages.SelectionMode = System.Windows.Forms.SelectionMode.None;
			this.listBoxMessages.Size = new System.Drawing.Size(300, 93);
			this.listBoxMessages.TabIndex = 12;
			// 
			// matrixControl1
			// 
			this.matrixControl1.ColsCount = 5;
			this.matrixControl1.ForeColor = System.Drawing.Color.LightPink;
			this.matrixControl1.Location = new System.Drawing.Point(13, 13);
			this.matrixControl1.Name = "matrixControl1";
			this.matrixControl1.RowsCount = 5;
			this.matrixControl1.Size = new System.Drawing.Size(300, 300);
			this.matrixControl1.Spacing = 4;
			this.matrixControl1.TabIndex = 0;
			// 
			// buttonNeuronGas
			// 
			this.buttonNeuronGas.Location = new System.Drawing.Point(406, 149);
			this.buttonNeuronGas.Name = "buttonNeuronGas";
			this.buttonNeuronGas.Size = new System.Drawing.Size(75, 23);
			this.buttonNeuronGas.TabIndex = 13;
			this.buttonNeuronGas.Text = "Neuron gas";
			this.buttonNeuronGas.UseVisualStyleBackColor = true;
			this.buttonNeuronGas.Click += new System.EventHandler(this.buttonNeuronGas_Click);
			// 
			// DigitForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(494, 442);
			this.Controls.Add(this.buttonNeuronGas);
			this.Controls.Add(this.listBoxMessages);
			this.Controls.Add(this.tbResult);
			this.Controls.Add(this.buttonClearWeights);
			this.Controls.Add(this.buttonLoadWeights);
			this.Controls.Add(this.buttonSaveWeights);
			this.Controls.Add(this.buttonWTA);
			this.Controls.Add(this.buttonRecognize);
			this.Controls.Add(this.buttonClearMatrix);
			this.Controls.Add(this.buttonLoadMatrix);
			this.Controls.Add(this.buttonSaveMatrix);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.matrixControl1);
			this.MinimumSize = new System.Drawing.Size(510, 480);
			this.Name = "DigitForm";
			this.Text = "DigitForm";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private JustNN.Lab2.MatrixControl matrixControl1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button buttonSaveMatrix;
		private System.Windows.Forms.Button buttonLoadMatrix;
		private System.Windows.Forms.Button buttonClearMatrix;
		private System.Windows.Forms.Button buttonRecognize;
		private System.Windows.Forms.Button buttonWTA;
		private System.Windows.Forms.Button buttonSaveWeights;
		private System.Windows.Forms.Button buttonLoadWeights;
		private System.Windows.Forms.Button buttonClearWeights;
		private System.Windows.Forms.TextBox tbResult;
		private System.Windows.Forms.ListBox listBoxMessages;
		private System.Windows.Forms.Button buttonNeuronGas;
	}
}