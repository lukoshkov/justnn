﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JustNN.Lab2;

namespace JustNN.Lab5
{
	public partial class DigitForm : Form
	{
		private Timer _timer;
		private float _time;
		private Lab2.Perceptron _perceptron;
		private double[,] _inputs;
		private double[,] _desired;
		private KohonenNetwork _network;
		private TrainingSetElement[] _trainingSet;

		public DigitForm()
		{
			InitializeComponent();
			var image = Image.FromFile("poemgame/s_sticker_2.png");
			pictureBox1.Image = image;
			UpdatePictureLocation();
			pictureBox1.Width = image.Width;
			pictureBox1.Height = image.Height;
			var filenames = System.IO.Directory.GetFiles("learning set");
			_perceptron = new Lab2.Perceptron(matrixControl1.RowsCount * matrixControl1.ColsCount + 1, filenames.Length);
			LoadLearningSet(filenames);
			var inputsCount = matrixControl1.RowsCount * matrixControl1.ColsCount;
			_network = new KohonenNetwork(inputsCount, filenames.Length);
			LoadTrainingSet(filenames, inputsCount);
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			//TestMatrixMultiply();
			tbResult.Text = "";
			tbResult.ReadOnly = true;
			tbResult.BackColor = BackColor;
			_timer = new Timer();
			_timer.Interval = 30;
			_timer.Tick += (s, ev) =>
			{
				UpdatePictureLocation();
				_time += 0.1f;
			};
			_timer.Start();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			UpdatePictureLocation();
		}

		private void UpdatePictureLocation()
		{
			var y = ClientSize.Height - pictureBox1.Height - 5 - (int)(20 * Math.Pow(Math.Sin(_time), 4));
			var x = ClientSize.Width - pictureBox1.Width - 10;
			pictureBox1.Location = new Point(x, y);
		}

		private void LoadTrainingSet(string[] filenames, int inputsCount)
		{
			_trainingSet = new TrainingSetElement[filenames.Length];
			for (int i = 0; i < filenames.Length; i++)
			{
				var tag = i.ToString();
				_trainingSet[i] = new TrainingSetElement(tag, inputsCount);
				_trainingSet[i].LoadInputs(filenames[i]);
				Vector.Normalize(_trainingSet[i].Inputs, _trainingSet[i].Inputs);
			}
		}

		private void LoadLearningSet(string[] filenames)
		{
			_inputs = new double[filenames.Length, _perceptron.InputsCount];
			_desired = new double[filenames.Length, filenames.Length];
			Matrix.Map(_desired, _desired, x => -1);
			var array = new double[_perceptron.InputsCount];
			var matrix = new double[matrixControl1.RowsCount, matrixControl1.ColsCount];
			array[array.Length - 1] = 1;
			for (int i = 0; i < filenames.Length; i++)
			{
				var binary = BinaryMatrix.Load(filenames[i]);
				Matrix.Map(binary, matrix, x => x ? 1 : -1);
				Matrix.ToArray(matrix, array, 0);
				for (int j = 0; j < array.Length; j++)
				{
					_inputs[i, j] = array[j];
				}
				_desired[i, i] = 1;
			}
		}

		private void buttonSaveMatrix_Click(object sender, EventArgs e)
		{
			var dialog = new SaveFileDialog();
			dialog.CheckPathExists = true;
			dialog.OverwritePrompt = true;
			dialog.DefaultExt = ".txt";
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				BinaryMatrix.Save(dialog.FileName, matrixControl1.GetMatrix());
			}
		}

		private void buttonLoadMatrix_Click(object sender, EventArgs e)
		{
			var dialog = new OpenFileDialog();
			dialog.CheckFileExists = true;
			dialog.Multiselect = false;
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				matrixControl1.SetMatrix(BinaryMatrix.Load(dialog.FileName));
			}
			tbResult.Text = "";
		}

		private void buttonClearMatrix_Click(object sender, EventArgs e)
		{
			matrixControl1.ClearMatrix();
			tbResult.Text = "";
		}

		private void buttonRecognize_Click(object sender, EventArgs e)
		{
			var inputs = matrixControl1.GetInputs();
			tbResult.Text = _network.Activate(inputs);
			//var outputs = _perceptron.Activate(inputs);
			//var sb = new StringBuilder();
			//for (int i = 0; i < outputs.Length; i++)
			//{
			//	if (outputs[i] > 0)
			//	{
			//		sb.Append(i);
			//	}
			//}
			//Text = sb.ToString();
			//tbResult.Text = Text.Length > 0 ? Text[0].ToString() : "";
		}

		private void buttonWTA_Click(object sender, EventArgs e)
		{
			_network.TrainWTA(_trainingSet, 1000);
			//TestNN();
			////Matrix.WriteToFile(_inputs, x => x.ToString(), " ", "x.txt");
			//var iter = _perceptron.HebbianLearning(_inputs, _desired, 1000);
			//listBoxMessages.Items.Add(string.Format(" Hebbian learning finished with {0} iterations", iter.ToString()));
			//tbResult.Text = "";
		}

		private void buttonNeuronGas_Click(object sender, EventArgs e)
		{
			_network.TrainGas(_trainingSet, 1000);
			//TestNN();
			//var iter = _perceptron.WidrowHoffLearning(_inputs, _desired, 1000);
			//listBoxMessages.Items.Add(string.Format(" Widrow-Hoff learning finished with {0} iterations", iter.ToString()));
			//tbResult.Text = "";
		}

		private void TestNN()
		{
			var good = true;
			var passes = 0;
			for (int i = 0; i < _trainingSet.Length; i++)
			{
				good = _network.Activate(_trainingSet[i].Inputs) == _trainingSet[i].Tag;
				if (good) passes++;
			}
			Text = (good ? "pass" : "not pass") + passes;
		}

		private void buttonSaveWeights_Click(object sender, EventArgs e)
		{
			var dialog = new SaveFileDialog();
			dialog.CheckPathExists = true;
			dialog.OverwritePrompt = true;
			dialog.DefaultExt = ".txt";
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				_perceptron.WriteWeights(dialog.FileName);
			}
		}

		private void buttonLoadWeights_Click(object sender, EventArgs e)
		{
			var dialog = new OpenFileDialog();
			dialog.CheckFileExists = true;
			dialog.Multiselect = false;
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				_perceptron.ReadWeights(dialog.FileName);
			}
			tbResult.Text = "";
		}

		private void buttonClearWeights_Click(object sender, EventArgs e)
		{
			_perceptron.ClearWeights();
			tbResult.Text = "";
		}
	}
}
