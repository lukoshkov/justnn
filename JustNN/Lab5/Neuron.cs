﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNN.Lab5
{
	public class Neuron
	{
		private double[] _weights;

		public string Tag { get; set; }

		public Neuron(int inputsCount)
		{
			_weights = new double[inputsCount];
		}

		public void InitWeights(Random random, double min, double max)
		{
			var diff = max - min;
			for (int i = 0; i < _weights.Length; i++)
			{
				_weights[i] = random.NextDouble() * diff + min;
			}
			NormalizeWeights();
		}

		public void ModifyWeights(double[] x, double factor)
		{
			for (int i = 0; i < _weights.Length; i++)
			{
				_weights[i] += factor * (x[i] - _weights[i]);
			}
			NormalizeWeights();
		}

		public void NormalizeWeights()
		{
			Vector.Normalize(_weights, _weights);
		}

		public double GetDistance(double[] x)
		{
			//return Vector.Dot(_weights, x) / Vector.GetLength(x) / Vector.GetLength(_weights);
			double s = 0;
			for (int i = 0; i < _weights.Length; i++)
			{
				var xi = x[i] - _weights[i];
				s += xi * xi;
			}
			return Math.Sqrt(s);
		}
	}
}
