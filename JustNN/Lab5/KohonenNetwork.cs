﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNN.Lab5
{
	public class KohonenNetwork
	{
		private Neuron[] _neurons;
		private bool[] _visited;
		private int[] _winCounts;
		private Random _random;

		public KohonenNetwork(int inputsCount, int neuronCount)
		{
			_neurons = new Neuron[neuronCount];
			for (int i = 0; i < neuronCount; i++)
			{
				_neurons[i] = new Neuron(inputsCount);
			}
			_random = new Random();
			_visited = new bool[neuronCount];
			_winCounts = new int[neuronCount];
		}

		public void InitWeights()
		{
			for (int i = 0; i < _neurons.Length; i++)
			{
				_neurons[i].InitWeights(_random, 0, 1);
			}
			for (int i = 0; i < _winCounts.Length; i++)
			{
				_winCounts[i] = 1;
			}
		}

		public string Activate(double[] inputs)
		{
			var distances = new double[_neurons.Length];
			GetDistances(inputs, distances);
			var winner = GetMinIndex(distances);
			return _neurons[winner].Tag;
		}

		public void TrainWTA(TrainingSetElement[] trainingSet, int maxIterations)
		{
			InitWeights();
			var sigma = 1;
			var factor = 0.1;
			var distances = new double[_neurons.Length];
			for (int iter = 0; iter < maxIterations; iter++)
			{
				for (int p = 0; p < trainingSet.Length; p++)
				{
					var x = trainingSet[p].Inputs;
					GetDistances(x, distances);
					var winner = GetMinIndex(distances);
					_visited[winner] = true;
					_winCounts[winner]++;
					_neurons[winner].ModifyWeights(x, factor);
					_neurons[winner].Tag = trainingSet[p].Tag;
				}
				for (int i = 0; i < _visited.Length; i++)
				{
					_visited[i] = false;
				}
			}
			for (int i = 0; i < _winCounts.Length; i++)
			{
				_winCounts[i] = 1;
			}
			//SetTags(trainingSet);
		}

		public void TrainGas(TrainingSetElement[] trainingSet, int maxIterations)
		{
			InitWeights();
			var sigmaMax = 10.0;
			var sigmaMin = 1.0;
			var sigma = sigmaMax;
			var factor = 0.1;
			var distances = new double[_neurons.Length];
			var order = new int[_neurons.Length];
			for (int iter = 0; iter < maxIterations; iter++)
			{
				sigma = sigmaMax * Math.Pow(sigmaMin / sigmaMax, (double)iter / maxIterations);
				for (int p = 0; p < trainingSet.Length; p++)
				{
					for (int i = 0; i < order.Length; i++)
					{
						order[i] = i;
					}
					var x = trainingSet[p].Inputs;
					GetDistances(x, distances);
					Array.Sort(distances, _neurons);
					for (int i = 0; i < _neurons.Length; i++)
					{
						var h = Math.Exp(-i / sigma);
						_neurons[i].ModifyWeights(x, factor * h);
					}
					_neurons[0].Tag = trainingSet[p].Tag;
				}
			}
		}

		private void SetTags(TrainingSetElement[] trainingSet)
		{
			var distances = new double[_neurons.Length];
			for (int i = 0; i < trainingSet.Length; i++)
			{
				var x = trainingSet[i].Inputs;
				GetDistances(x, distances);
				var winner = GetMinIndex(distances);
				_neurons[winner].Tag = trainingSet[i].Tag;
			}
		}

		private void GetDistances(double[] x, double[] distances)
		{
			for (int i = 0; i < _neurons.Length; i++)
			{
				//distances[i] = _visited[i] ? 10000 : _neurons[i].GetDistance(x);
				distances[i] = _winCounts[i] * _neurons[i].GetDistance(x);
			}
		}

		private int GetMinIndex(double[] distances)
		{
			var minI = 0;
			for (int i = 1; i < distances.Length; i++)
			{
				if (distances[i] < distances[minI])
				{
					minI = i;
				}
			}
			return minI;
		}
	}
}
