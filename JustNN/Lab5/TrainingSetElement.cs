﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNN.Lab5
{
	public class TrainingSetElement
	{
		public string Tag { get; set; }
		public double[] Inputs { get; private set; }

		public TrainingSetElement(string tag, int inputsCount)
		{
			Tag = tag;
			Inputs = new double[inputsCount];
		}

		public void LoadInputs(string filename)
		{
			var binary = Lab2.BinaryMatrix.Load(filename);
			int k = 0;
			for (int i = 0; i < binary.GetLength(0); i++)
			{
				for (int j = 0; j < binary.GetLength(1); j++)
				{
					Inputs[k] = binary[i, j] ? 1 : 0;
					k++;
				}
			}
		}
	}
}
