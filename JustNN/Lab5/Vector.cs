﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNN.Lab5
{
	public static class Vector
	{
		public static double GetLength(double[] v)
		{
			double s = 0;
			for (int i = 0; i < v.Length; i++)
			{
				s += v[i] * v[i];
			}
			return Math.Sqrt(s);
		}

		public static void Join(double[] v1, double[] v2, double[] result, Func<double, double, double> joinFunc)
		{
			for (int i = 0; i < result.Length; i++)
			{
				result[i] = joinFunc(v1[i], v2[i]);
			}
		}

		public static void Add(double[] v1, double[] v2, double[] result)
		{
			for (int i = 0; i < result.Length; i++)
			{
				result[i] = v1[i] + v2[i];
			}
		}

		public static void Sub(double[] v1, double[] v2, double[] result)
		{
			for (int i = 0; i < result.Length; i++)
			{
				result[i] = v1[i] - v2[i];
			}
		}

		public static double Dot(double[] v1, double[] v2)
		{
			double s = 0;
			for (int i = 0; i < v1.Length; i++)
			{
				s += v1[i] * v2[i];
			}
			return s;
		}

		public static void Scale(double[] v, double[] result, double factor)
		{
			for (int i = 0; i < v.Length; i++)
			{
				result[i] = v[i] * factor;
			}
		}

		public static void Normalize(double[] v, double[] result)
		{
			Scale(v, result, 1 / GetLength(v));
		}
	}
}
