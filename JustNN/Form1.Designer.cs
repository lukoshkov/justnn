﻿namespace JustNN
{
	partial class Form1
	{
		/// <summary>
		/// Требуется переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Обязательный метод для поддержки конструктора - не изменяйте
		/// содержимое данного метода при помощи редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.coordinatePlane1 = new JustNN.CoordinatePlane();
			this.SuspendLayout();
			// 
			// coordinatePlane1
			// 
			this.coordinatePlane1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.coordinatePlane1.GridVisible = true;
			this.coordinatePlane1.Location = new System.Drawing.Point(12, 12);
			this.coordinatePlane1.Name = "coordinatePlane1";
			this.coordinatePlane1.PointsA = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("coordinatePlane1.PointsA")));
			this.coordinatePlane1.PointsB = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("coordinatePlane1.PointsB")));
			this.coordinatePlane1.Size = new System.Drawing.Size(560, 538);
			this.coordinatePlane1.TabIndex = 0;
			this.coordinatePlane1.TagsVisible = true;
			this.coordinatePlane1.XSize = 10F;
			this.coordinatePlane1.YSize = 10F;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(584, 562);
			this.Controls.Add(this.coordinatePlane1);
			this.Name = "Form1";
			this.Text = "Learning process";
			this.ResumeLayout(false);

		}

		#endregion

		private CoordinatePlane coordinatePlane1;
	}
}

