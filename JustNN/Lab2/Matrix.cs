﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNN.Lab2
{
	public static class Matrix
	{
		#region Generic methods

		public static T[,] Multiply<T>(T[,] left, T[,] right, Func<T, T, T> add, Func<T, T, T> mul)
		{
			if (left.GetLength(1) != right.GetLength(0))
			{
				throw new ArgumentOutOfRangeException();
			}

			var result = new T[left.GetLength(0), right.GetLength(1)];
			return Multiply(left, right, result, add, mul);
		}

		public static T[,] Multiply<T>(T[,] left, T[,] right, T[,] result, Func<T, T, T> add, Func<T, T, T> mul)
		{
			if (left.GetLength(1) != right.GetLength(0) || left.GetLength(0) != result.GetLength(0) || right.GetLength(1) != result.GetLength(1))
			{
				throw new ArgumentOutOfRangeException();
			}
			
			for (int i = 0; i < left.GetLength(0); i++)
			{
				for (int j = 0; j < right.GetLength(1); j++)
				{
					for (int k = 0; k < left.GetLength(1); k++)
					{
						result[i, j] = add(result[i, j], mul(left[i, k], right[k, j]));
					}
				}
			}

			return result;
		}

		public static T[,] Add<T>(T[,] left, T[,] right, Func<T, T, T> add)
		{
			var rc = left.GetLength(0);
			var cc = left.GetLength(1);
			if (rc != right.GetLength(0) || cc != right.GetLength(1))
			{
				throw new ArgumentOutOfRangeException();
			}
			return Add(left, right, new T[rc, cc], add);
		}

		public static T[,] Add<T>(T[,] left, T[,] right, T[,] result, Func<T, T, T> add)
		{
			var rc = left.GetLength(0);
			var cc = left.GetLength(1);
			if (rc != right.GetLength(0) || rc != result.GetLength(0) || cc != right.GetLength(1) || cc != result.GetLength(1))
			{
				throw new ArgumentOutOfRangeException();
			}
			
			for (int i = 0; i < rc; i++)
			{
				for (int j = 0; j < cc; j++)
				{
					result[i, j] = add(left[i, j], right[i, j]);
				}
			}

			return result;
		}

		public static T[,] Transpose<T>(T[,] matrix)
		{
			var rc = matrix.GetLength(0);
			var cc = matrix.GetLength(1);
			var t = new T[cc, rc];
			for (int i = 0; i < rc; i++)
			{
				for (int j = 0; j < cc; j++)
				{
					t[j, i] = matrix[i, j];
				}
			}
			return t;
		}

		public static T2[,] Map<T1, T2>(T1[,] src, T2[,] dst, Func<T1, T2> map)
		{
			var rc = dst.GetLength(0);
			var cc = dst.GetLength(1);
			if (src.GetLength(0) != rc || src.GetLength(1) != cc)
			{
				throw new ArgumentOutOfRangeException();
			}

			for (int i = 0; i < rc; i++)
			{
				for (int j = 0; j < cc; j++)
				{
					dst[i, j] = map(src[i, j]);
				}
			}
			return dst;
		}

		public static T[,] Copy<T>(T[,] src)
		{
			var rc = src.GetLength(0);
			var cc = src.GetLength(1);
			var result = new T[rc, cc];
			for (int i = 0; i < rc; i++)
			{
				for (int j = 0; j < cc; j++)
				{
					result[i, j] = src[i, j];
				}
			}
			return result;
		}

		public static T[] ToArray<T>(T[,] src, T[] dst, int startIndex)
		{
			var rc = src.GetLength(0);
			var cc = src.GetLength(1);
			if (rc * cc > dst.Length - startIndex)
			{
				throw new ArgumentOutOfRangeException();
			}
			int k = startIndex;
			for (int i = 0; i < rc; i++)
			{
				for (int j = 0; j < cc; j++)
				{
					dst[k++] = src[i, j];
				}
			}
			return dst;
		}

		public static T[,] FromArray<T>(T[] src, T[,] dst, int startIndex)
		{
			var rc = src.GetLength(0);
			var cc = src.GetLength(1);
			if (rc * cc < dst.Length - startIndex)
			{
				throw new ArgumentOutOfRangeException();
			}
			int k = startIndex;
			for (int i = 0; i < rc; i++)
			{
				for (int j = 0; j < cc; j++)
				{
					dst[i, j] = src[k++];
				}
			}
			return dst;
		}

		public static string ToString<T>(T[,] matrix, Func<T, string> toString, string separator)
		{
			var sb = new StringBuilder();
			for (int i = 0; i < matrix.GetLength(0); i++)
			{
				sb.Append(toString(matrix[i, 0]));
				for (int j = 1; j < matrix.GetLength(1); j++)
				{
					sb.Append(separator);
					sb.Append(toString(matrix[i, j]));
				}
				sb.Append('\n');
			}
			return sb.ToString();
		}

		public static void WriteToFile<T>(T[,] matrix, Func<T, string> toString, string separator, string filename)
		{
			using (var file = new System.IO.StreamWriter(filename))
			{
				file.WriteLine(matrix.GetLength(0));
				file.WriteLine(matrix.GetLength(1));
				for (int i = 0; i < matrix.GetLength(0); i++)
				{
					file.Write(toString(matrix[i, 0]));
					for (int j = 1; j < matrix.GetLength(1); j++)
					{
						file.Write(separator);
						file.Write(toString(matrix[i, j]));
					}
					file.WriteLine();
				}
			}
		}

		public static T[,] ReadFromFile<T>(Func<string, T> parse, string separator, string filename)
		{
			using (var file = new System.IO.StreamReader(filename))
			{
				var rc = int.Parse(file.ReadLine());
				var cc = int.Parse(file.ReadLine());
				var matrix = new T[rc, cc];
				for (int i = 0; i < rc; i++)
				{
					var line = file.ReadLine().Split(separator.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
					for (int j = 1; j < cc; j++)
					{
						matrix[i, j] = parse(line[j]);
					}
				}
				return matrix;
			}
		}

		#endregion

		#region double methods

		public static double[,] Multiply(double[,] left, double[,] right)
		{
			if (left.GetLength(1) != right.GetLength(0))
			{
				throw new ArgumentOutOfRangeException();
			}

			var result = new double[left.GetLength(0), right.GetLength(1)];
			for (int i = 0; i < left.GetLength(0); i++)
			{
				for (int j = 0; j < right.GetLength(1); j++)
				{
					for (int k = 0; k < left.GetLength(1); k++)
					{
						result[i, j] += left[i, k] * right[k, j];
					}
				}
			}

			return result;
		}

		public static double[] Multiply(double[] left, double[,] right)
		{
			if (left.Length != right.GetLength(0))
			{
				throw new ArgumentOutOfRangeException();
			}

			var result = new double[right.GetLength(1)];
			for (int i = 0; i < result.Length; i++)
			{
				for (int j = 0; j < left.Length; j++)
				{
					result[i] += left[j] * right[j, i];
				}
			}

			return result;
		}
		
		public static double[] Multiply(double[,] left, double[] right)
		{
			if (left.GetLength(1) != right.Length)
			{
				throw new ArgumentOutOfRangeException();
			}

			var result = new double[left.GetLength(0)];
			for (int i = 0; i < result.Length; i++)
			{
				for (int j = 0; j < left.Length; j++)
				{
					result[i] += left[i, j] * right[j];
				}
			}

			return result;
		}

		public static double[,] Multiply(double[] left, double[] right)
		{
			var result = new double[left.Length, right.Length];
			for (int i = 0; i < left.Length; i++)
			{
				for (int j = 0; j < right.Length; j++)
				{
					result[i, j] = left[i] * right[j];
				}
			}

			return result;
		}

		#endregion
	}
}
