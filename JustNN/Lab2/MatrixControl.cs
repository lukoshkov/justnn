﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JustNN.Lab2
{
	public partial class MatrixControl : UserControl
	{
		private int _spacing;
		private int _rowsCount;
		private int _colsCount;
		private bool[,] _matrix;

		private Brush _checkedCellBrush;
		private Brush _uncheckedCellBrush;

		private CellGeometry _cellShape;

		public enum CellGeometry
		{
			Ellipse,
			Rectangle
		}

		public CellGeometry CellShape
		{
			get { return _cellShape; }
			set
			{
				if (_cellShape != value)
				{
					_cellShape = value;
					Invalidate();
				}
			}
		}

		public int Spacing
		{
			get { return _spacing; }
			set
			{
				_spacing = value > 0 ? value : _spacing;
				Invalidate();
			}
		}

		public int RowsCount
		{
			get { return _rowsCount; }
			set
			{
				_rowsCount = value > 0 ? value : _rowsCount;
				Matrix = new bool[RowsCount, ColsCount];
			}
		}

		public int ColsCount
		{
			get { return _colsCount; }
			set
			{
				_colsCount = value > 0 ? value : _colsCount;
				Matrix = new bool[RowsCount, ColsCount];
			}
		}

		private bool[,] Matrix
		{
			get { return _matrix; }
			set
			{
				_matrix = value;
				Invalidate();
			}
		}

		public MatrixControl()
		{
			InitializeComponent();
			_checkedCellBrush = Brushes.Black;
			_uncheckedCellBrush = Brushes.White;
			Matrix = new bool[RowsCount, ColsCount];
			DoubleBuffered = true;
			Invalidate();
		}

		public bool[,] GetMatrix()
		{
			return _matrix;
		}

		public void SetMatrix(bool[,] matrix)
		{
			Matrix = matrix;
		}

		public void ClearMatrix()
		{
			for (int row = 0; row < RowsCount; row++)
			{
				for (int col = 0; col < ColsCount; col++)
				{
					_matrix[row, col] = false;
				}
			}
			Invalidate();
		}

		public double[] GetInputs()
		{
			var inputs = new double[RowsCount * ColsCount + 1];
			for (int row = 0; row < RowsCount; row++)
			{
				for (int col = 0; col < ColsCount; col++)
				{
					var i = row * RowsCount + col;
					inputs[i] = Matrix[row, col] ? 1 : -1;
				}
			}
			inputs[inputs.Length - 1] = 1;
			return inputs;
		}

		public void SetInputs(int[] inputs)
		{
			if (inputs.Length >= RowsCount * ColsCount)
			{
				for (int row = 0; row < RowsCount; row++)
				{
					for (int col = 0; col < ColsCount; col++)
					{
						var i = row * RowsCount + col;
						Matrix[row, col] = inputs[i] == 1;
					}
				}
				Invalidate();
			}
		}

		protected override void OnForeColorChanged(EventArgs e)
		{
			base.OnForeColorChanged(e);
			_checkedCellBrush = new SolidBrush(ForeColor);
		}

		protected override void OnBackColorChanged(EventArgs e)
		{
			base.OnBackColorChanged(e);
			_uncheckedCellBrush = new SolidBrush(BackColor);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			var g = e.Graphics;
			var dx = Width / ColsCount;
			var dy = Height / RowsCount;
			var cellW = dx - 2 * _spacing;
			var cellH = dy - 2 * _spacing;

			for (int row = 0; row < RowsCount; row++)
			{
				for (int col = 0; col < ColsCount; col++)
				{
					var brush = Matrix[row, col] ? _checkedCellBrush : _uncheckedCellBrush;
					if (_cellShape == CellGeometry.Ellipse)
					{
						g.FillEllipse(brush, col * dx + _spacing, row * dy + _spacing, cellW, cellH);
					}
					else
					{
						g.FillRectangle(brush, col * dx + _spacing, row * dy + _spacing, cellW, cellH);
					}
				}
			}
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			Invalidate();
		}

		protected override void OnMouseClick(MouseEventArgs e)
		{
			base.OnMouseClick(e);
			var col = e.X * RowsCount / Width;
			var row = e.Y * ColsCount / Height;
			Matrix[row, col] = !Matrix[row, col];
			Invalidate();
		}
	}
}
