﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNN.Lab2
{
	public class Perceptron
	{
		private static readonly Func<double, double, double> _add = (a, b) => a + b;
		private static readonly Func<double, double, double> _sub = (a, b) => a - b;
		private static readonly Func<double, double, double> _mul = (a, b) => a * b;

		private double _constant = 0.01;
		private double[,] _weights;
		private Func<double, double> _activationFunc;

		public int InputsCount { get; private set; }
		public int OutputsCount { get; private set; }

		public Func<double, double> ActivationFunc
		{
			set
			{
				if (value != null)
				{
					_activationFunc = value;
				}
			}
		}

		public Perceptron(int inputsCount, int outputsCount)
		{
			_weights = new double[inputsCount, outputsCount];
			InputsCount = inputsCount;
			OutputsCount = outputsCount;
			//var random = new Random();
			//for (int i = 0; i < inputsCount; i++)
			//{
			//	for (int j = 0; j < outputsCount; j++)
			//	{
			//		_weights[i, j] = (float)(random.NextDouble() * 2 - 1);
			//	}
			//}
			_activationFunc = x => x > 0 ? 1 : -1;
		}

		public double[] Activate(double[] inputs)
		{
			var outputs = Matrix.Multiply(inputs, _weights);
			for (int i = 0; i < OutputsCount; i++)
			{
				outputs[i] = _activationFunc(outputs[i]);
			}
			return outputs;
		}

		public int HebbianLearning(double[,] inputs, double[,] desired, int maxIterations)
		{
			var outputs = Activate(inputs);
			var error = new double[desired.GetLength(0), desired.GetLength(1)];
			var xt = Matrix.Transpose(inputs);
			Matrix.Map(xt, xt, x => x * _constant);
			var w = new double[_weights.GetLength(0), _weights.GetLength(1)];
			for (int i = 0; i < maxIterations; i++)
			{
				Matrix.Add(desired, outputs, error, _sub);
				if (GetAbsSum(error) < 1)
				{
					return i;
				}
				Matrix.Multiply(xt, error, w, _add, _mul);
				Matrix.Add(_weights, w, _weights, _add);
				outputs = Activate(inputs);
			}
			return maxIterations;
		}

		public int WidrowHoffLearning(double[,] inputs, double[,] desired, int maxIterations)
		{
			var outputs = Activate(inputs);
			var error = new double[desired.GetLength(0), desired.GetLength(1)];
			var xI = Matrix.Copy(inputs);
			xI = Matrix.Transpose(inputs);
			Matrix.Map(xI, xI, x => x * _constant);
			var w = new double[_weights.GetLength(0), _weights.GetLength(1)];
			for (int i = 0; i < maxIterations; i++)
			{
				Matrix.Add(desired, outputs, error, _sub);
				if (CalcE(error) < 0.001)
				{
					return i;
				}
				Matrix.Multiply(xI, error, w, _add, _mul);
				Matrix.Add(_weights, w, _weights, _add);
				outputs = Activate(inputs);
			}
			return maxIterations;
		}

		private double GetAbsSum(double[,] a)
		{
			var s = 0.0;
			for (int i = 0; i < a.GetLength(0); i++)
			{
				for (int j = 0; j < a.GetLength(1); j++)
				{
					s += Math.Abs(a[i, j]);
				}
			}
			return s;
		}

		private double[,] Activate(double[,] inputs)
		{
			var outputs = Matrix.Multiply(inputs, _weights, _add, _mul);
			return Matrix.Map(outputs, outputs, s => _activationFunc(s));
		}

		private double CalcE(double[,] diff)
		{
			var s = 0.0;
			Func<double, double> sum = x => { s += x * x; return x; };
			Matrix.Map(diff, diff, sum);
			return s / 2;
		}

		public string WeightsToString()
		{
			return Matrix.ToString(_weights, x => string.Format("{0:f3}", x), "\t");
		}

		public void WriteWeights(string filename)
		{
			Matrix.WriteToFile(_weights, x => string.Format("{0:f3}", x), "\t", filename);
		}

		public void ReadWeights(string filename)
		{
			_weights = Matrix.ReadFromFile(x => double.Parse(x), "\t", filename);
		}

		public void ClearWeights()
		{
			Matrix.Map(_weights, _weights, x => 0);
		}
	}
}
