﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNN.Lab2
{
	public class BinaryMatrix
	{
		public static bool[,] Load(string filename)
		{
			var lines = System.IO.File.ReadAllLines(filename);
			var line = lines[0].Split(' ');
			var matrix = new bool[lines.Length, line.Length];
			for (int i = 0; i < lines.Length; i++)
			{
				line = lines[i].Split(' ');
				for (int j = 0; j < line.Length; j++)
				{
					int value;
					int.TryParse(line[j], out value);
					matrix[i, j] = value == 1;
				}
			}
			return matrix;
		}

		public static void Save(string filename, bool[,] matrix)
		{
			using (var file = new System.IO.StreamWriter(filename))
			{
				for (int i = 0; i < matrix.GetLength(0); i++)
				{
					file.Write(matrix[i, 0] ? 1 : 0);
					for (int j = 1; j < matrix.GetLength(1); j++)
					{
						file.Write(" {0}", matrix[i, j] ? 1 : 0);
					}
					file.WriteLine();
				}
			}
		}
	}
}
