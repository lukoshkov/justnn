﻿namespace JustNN.Lab6
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonRecognize = new System.Windows.Forms.Button();
			this.buttonLoadX = new System.Windows.Forms.Button();
			this.buttonClear = new System.Windows.Forms.Button();
			this.buttonTeach = new System.Windows.Forms.Button();
			this.tbResult = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonSaveX = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.matrixControl1 = new JustNN.Lab2.MatrixControl();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// buttonRecognize
			// 
			this.buttonRecognize.Location = new System.Drawing.Point(635, 131);
			this.buttonRecognize.Name = "buttonRecognize";
			this.buttonRecognize.Size = new System.Drawing.Size(118, 23);
			this.buttonRecognize.TabIndex = 1;
			this.buttonRecognize.Text = "Recognize";
			this.buttonRecognize.UseVisualStyleBackColor = true;
			this.buttonRecognize.Click += new System.EventHandler(this.buttonRecognize_Click);
			// 
			// buttonLoadX
			// 
			this.buttonLoadX.Location = new System.Drawing.Point(635, 44);
			this.buttonLoadX.Name = "buttonLoadX";
			this.buttonLoadX.Size = new System.Drawing.Size(118, 23);
			this.buttonLoadX.TabIndex = 2;
			this.buttonLoadX.Text = "Load matrix";
			this.buttonLoadX.UseVisualStyleBackColor = true;
			this.buttonLoadX.Click += new System.EventHandler(this.buttonLoadX_Click);
			// 
			// buttonClear
			// 
			this.buttonClear.Location = new System.Drawing.Point(635, 102);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(118, 23);
			this.buttonClear.TabIndex = 3;
			this.buttonClear.Text = "Clear matrix";
			this.buttonClear.UseVisualStyleBackColor = true;
			this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
			// 
			// buttonTeach
			// 
			this.buttonTeach.Location = new System.Drawing.Point(635, 15);
			this.buttonTeach.Name = "buttonTeach";
			this.buttonTeach.Size = new System.Drawing.Size(118, 23);
			this.buttonTeach.TabIndex = 4;
			this.buttonTeach.Text = "Train";
			this.buttonTeach.UseVisualStyleBackColor = true;
			this.buttonTeach.Click += new System.EventHandler(this.buttonTeach_Click);
			// 
			// tbResult
			// 
			this.tbResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbResult.Location = new System.Drawing.Point(635, 191);
			this.tbResult.Name = "tbResult";
			this.tbResult.ReadOnly = true;
			this.tbResult.Size = new System.Drawing.Size(118, 26);
			this.tbResult.TabIndex = 5;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(632, 175);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(40, 13);
			this.label1.TabIndex = 6;
			this.label1.Text = "Result:";
			// 
			// buttonSaveX
			// 
			this.buttonSaveX.Location = new System.Drawing.Point(635, 73);
			this.buttonSaveX.Name = "buttonSaveX";
			this.buttonSaveX.Size = new System.Drawing.Size(118, 23);
			this.buttonSaveX.TabIndex = 7;
			this.buttonSaveX.Text = "Save matrix";
			this.buttonSaveX.UseVisualStyleBackColor = true;
			this.buttonSaveX.Click += new System.EventHandler(this.buttonSaveX_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new System.Drawing.Point(663, 563);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(100, 50);
			this.pictureBox1.TabIndex = 8;
			this.pictureBox1.TabStop = false;
			// 
			// matrixControl1
			// 
			this.matrixControl1.CellShape = JustNN.Lab2.MatrixControl.CellGeometry.Ellipse;
			this.matrixControl1.ColsCount = 20;
			this.matrixControl1.ForeColor = System.Drawing.Color.Pink;
			this.matrixControl1.Location = new System.Drawing.Point(13, 13);
			this.matrixControl1.Name = "matrixControl1";
			this.matrixControl1.RowsCount = 20;
			this.matrixControl1.Size = new System.Drawing.Size(600, 600);
			this.matrixControl1.Spacing = 1;
			this.matrixControl1.TabIndex = 0;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(775, 625);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.buttonSaveX);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.tbResult);
			this.Controls.Add(this.buttonTeach);
			this.Controls.Add(this.buttonClear);
			this.Controls.Add(this.buttonLoadX);
			this.Controls.Add(this.buttonRecognize);
			this.Controls.Add(this.matrixControl1);
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Name = "Form1";
			this.Text = "RBF Network";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Lab2.MatrixControl matrixControl1;
		private System.Windows.Forms.Button buttonRecognize;
		private System.Windows.Forms.Button buttonLoadX;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Button buttonTeach;
		private System.Windows.Forms.TextBox tbResult;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonSaveX;
		private System.Windows.Forms.PictureBox pictureBox1;
	}
}