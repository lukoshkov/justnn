﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNN.Lab6
{
	public class RbfNetwork
	{
		public double[][] C;
		public double[,] H;
		public double[,] Y;
		public double[,] G;
		public double[,] W;
		public double sigmaSqrTwice = 10;
		public double[] Sigma;

		public RbfNetwork(double[][] inputVectors, List<int> _inputValues, int countFb = 10)
		{
			var step = inputVectors.GetLength(0) / countFb;
			C = new double[countFb][];
			for (var j = 0; j < countFb; j++)
			{
				C[j] = inputVectors[j * step];
			}

			Sigma = new double[countFb];
			for (int i = 0; i < countFb; i++)
			{
				var minDist = 1000000.0;
				for (int j = 0; j < countFb; j++)
				{
					if (i != j)
					{
						var dist = EuclideanDistance(C[i], C[j]);
						if (dist < minDist)
						{
							minDist = dist;
						}
					}
				}
				Sigma[i] = minDist * minDist * 2;
			}

			H = new double[inputVectors.GetLength(0), countFb];
			for (var i = 0; i < inputVectors.GetLength(0); i++)
			{
				for (var j = 0; j < countFb; j++)
				{
					//H[i, j] = Gauss(inputVectors[i], C[j], sigmaSqrTwice);
					H[i, j] = Gauss(inputVectors[i], j);
				}
			}
			Y = new double[_inputValues.Count, 1];
			for (int i = 0; i < _inputValues.Count; i++)
			{
				for (int j = 0; j < 1; j++)
				{
					Y[i, j] = _inputValues[i];
				}
			}
			var t = Transpose(H);
			G = MultiplyMatrix(Inverse(MultiplyMatrix(t, H)), t);
			W = MultiplyMatrix(G, Y);
			for (var i = 0; i < inputVectors.GetLength(0); i++)
			{
				var result = RecognizeValue(inputVectors[i]);
				System.Diagnostics.Debug.WriteLine("Class {0}, Test value {1:f3}", i, result);
			}
		}

		public double RecognizeValue(double[] inputVector)
		{
			var result = 0.0;
			for (var j = 0; j < W.GetLength(0); j++)
			{
				//result += W[j, 0] * Gauss(inputVector, C[j], sigmaSqrTwice);
				result += W[j, 0] * Gauss(inputVector, j);
			}
			return result;
		}

		public double GetSigmoidResult(double[] _weights, double[] input)
		{
			return SigmoidFunction(_weights.Select((t, i) => t * input[i]).Sum(), 1);
		}

		private double SigmoidFunction(double x, double k)
		{
			return 1 / (1 + Math.Exp(-k * x));
		}

		private double EuclideanDistance(double[] inputVector, double[] centerRadial)
		{
			var sum = inputVector.Select((t, i) => Math.Pow(t - centerRadial[i], 2)).Sum();
			return Math.Sqrt(sum);
		}

		private double Gauss(double[] inputVector, int centerIndex)
		{
			return Gauss(inputVector, C[centerIndex], Sigma[centerIndex]);
		}

		private double Gauss(double[] inputVector, double[] centerRadial, double sigmaSqrTwice)
		{
			double distSqr = 0;
			for (int i = 0; i < centerRadial.Length; i++)
			{
				var xi = inputVector[i] - centerRadial[i];
				distSqr += xi * xi;
			}
			return Math.Exp(-distSqr / sigmaSqrTwice);
		}

		private double[,] Transpose(double[,] matrix)
		{
			int w = matrix.GetLength(0);
			int h = matrix.GetLength(1);
			double[,] result = new double[h, w];
			for (int i = 0; i < w; i++)
			{
				for (int j = 0; j < h; j++)
				{
					result[j, i] = matrix[i, j];
				}
			}
			return result;
		}

		public double[,] MultiplyMatrix(double[,] a, double[,] b)
		{
			var m1 = a.GetLength(0);
			var m2 = a.GetLength(1);
			var n1 = b.GetLength(0);
			var n2 = b.GetLength(1);
			var c = new double[m1, n2];
			for (int i = 0; i < m1; i++)
			{
				for (int j = 0; j < n2; j++)
				{
					c[i, j] = 0;
					for (int k = 0; k < n1; k++) c[i, j] = c[i, j] + a[i, k] * b[k, j];
				}
			}
			return c;
		}

		private static double MatrixDeterminant(double[,] matrix)
		{
			int[] perm;
			int toggle;
			double[,] lum = MatrixDecompose(matrix, out perm, out toggle);
			if (lum == null)
				throw new Exception("Unable to compute MatrixDeterminant");
			double result = toggle;
			for (int i = 0; i < lum.GetLength(0); ++i)
				result *= lum[i, i];

			return result;
		}

		private static double[,] MatrixDecompose(double[,] matrix, out int[] perm, out int toggle)
		{
			// Doolittle LUP decomposition with partial pivoting.
			// rerturns: result is L (with 1s on diagonal) and U; perm holds row permutations; toggle is +1 or -1 (even or odd)
			int rows = matrix.GetLength(0);
			int cols = matrix.GetLength(1);

			//Check if matrix is square
			if (rows != cols)
				throw new Exception("Attempt to MatrixDecompose a non-square mattrix");

			double[,] result = MatrixDuplicate(matrix); // make a copy of the input matrix

			perm = new int[rows]; // set up row permutation result
			for (int i = 0; i < rows; ++i) { perm[i] = i; } // i are rows counter

			toggle = 1; // toggle tracks row swaps. +1 -> even, -1 -> odd. used by MatrixDeterminant

			for (int j = 0; j < rows - 1; ++j) // each column, j is counter for coulmns
			{
				double colMax = Math.Abs(result[j, j]); // find largest value in col j
				int pRow = j;
				for (int i = j + 1; i < rows; ++i)
				{
					if (result[i, j] > colMax)
					{
						colMax = result[i, j];
						pRow = i;
					}
				}

				if (pRow != j) // if largest value not on pivot, swap rows
				{
					double[] rowPtr = new double[result.GetLength(1)];

					//in order to preserve value of j new variable k for counter is declared
					//rowPtr[] is a 1D array that contains all the elements on a single row of the matrix
					//there has to be a loop over the columns to transfer the values
					//from the 2D array to the 1D rowPtr array.
					//----tranfer 2D array to 1D array BEGIN

					for (int k = 0; k < result.GetLength(1); k++)
					{
						rowPtr[k] = result[pRow, k];
					}

					for (int k = 0; k < result.GetLength(1); k++)
					{
						result[pRow, k] = result[j, k];
					}

					for (int k = 0; k < result.GetLength(1); k++)
					{
						result[j, k] = rowPtr[k];
					}

					//----tranfer 2D array to 1D array END

					int tmp = perm[pRow]; // and swap perm info
					perm[pRow] = perm[j];
					perm[j] = tmp;

					toggle = -toggle; // adjust the row-swap toggle
				}

				//if (Math.Abs(result[j, j]) < 1.0E-30) // if diagonal after swap is zero . . .
				//    return null; // consider a throw

				for (int i = j + 1; i < rows; ++i)
				{
					result[i, j] /= result[j, j];
					for (int k = j + 1; k < rows; ++k)
					{
						result[i, k] -= result[i, j] * result[j, k];
					}
				}
			} // main j column loop

			return result;
		} // MatrixDecompose

		private static double[,] MatrixDuplicate(double[,] matrix)
		{
			// allocates/creates a duplicate of a matrix. assumes matrix is not null.
			double[,] result = MatrixCreate(matrix.GetLength(0), matrix.GetLength(1));
			for (int i = 0; i < matrix.GetLength(0); ++i) // copy the values
				for (int j = 0; j < matrix.GetLength(1); ++j)
					result[i, j] = matrix[i, j];
			return result;
		}

		private static double[,] MatrixCreate(int rows, int cols)
		{
			// allocates/creates a matrix initialized to all 0.0. assume rows and cols > 0
			// do error checking here
			double[,] result = new double[rows, cols];
			return result;
		}

		private static double[,] Exclude(double[,] mA, int row, int col)
		{
			double[,] result = new double[mA.GetLength(0) - 1, mA.GetLength(1) - 1];
			for (int i = 0; i < result.GetLength(0); i++)
			{
				var newRow = 0;
				if (i >= row)
				{
					newRow = 1;
				}
				for (int j = 0; j < result.GetLength(1); j++)
				{
					var newColumn = 0;
					if (j >= col)
					{
						newColumn = 1;
					}
					result[i, j] = mA[i + newRow, j + newColumn];
				}
			}
			return result;
		}

		private static double[,] Inverse(double[,] mA, uint round = 0)
		{
			if (mA.GetLength(0) != mA.GetLength(1)) throw new ArgumentException("Обратная матрица существует только для квадратных, невырожденных, матриц.");
			double[,] matrix = MatrixCreate(mA.GetLength(0), mA.GetLength(0)); //Делаем копию исходной матрицы
			double determinant = MatrixDeterminant(mA); //Находим детерминант

			if (determinant == 0) return matrix; //Если определитель == 0 - матрица вырожденная

			for (int i = 0; i < mA.GetLength(0); i++)
			{
				for (int t = 0; t < mA.GetLength(1); t++)
				{
					double[,] tmp = Exclude(mA, i, t);  //получаем матрицу без строки i и столбца t
														//(1 / determinant) * Determinant(tmp) - формула поределения элемента обратной матрицы
					matrix[t, i] = round == 0 ? (1 / determinant) * Math.Pow(-1, i + t) * MatrixDeterminant(tmp) : Math.Round(((1 / determinant) * Math.Pow(-1, i + t) * MatrixDeterminant(tmp)), (int)round, MidpointRounding.ToEven);
				}
			}
			return matrix;
		}
	}
}
