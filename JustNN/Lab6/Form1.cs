﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JustNN.Lab6
{
	public partial class Form1 : Form
	{
		private Timer _timer;
		private float _time;
		private RbfNetwork _net;
		private double[][] _inputVector;
		private List<int> _inputValues = new List<int>();
		private List<string> _tags;

		public Form1()
		{
			InitializeComponent();
			var image = Image.FromFile("poemgame/s_sticker_2.png");
			Click += (s, e) => pictureBox1.Visible = !pictureBox1.Visible;
			//pictureBox1.Visible = false;
			pictureBox1.Image = image;
			UpdatePictureLocation();
			pictureBox1.Width = image.Width;
			pictureBox1.Height = image.Height;
			_inputVector = LoadInputVector();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			tbResult.Text = "";
			tbResult.ReadOnly = true;
			tbResult.BackColor = BackColor;
			_timer = new Timer();
			_timer.Interval = 30;
			_timer.Tick += (s, ev) =>
			{
				UpdatePictureLocation();
				_time += 0.1f;
			};
			_timer.Start();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			UpdatePictureLocation();
		}

		private void UpdatePictureLocation()
		{
			var y = ClientSize.Height - pictureBox1.Height - 5 - (int)(20 * Math.Pow(Math.Sin(_time), 4));
			var x = ClientSize.Width - pictureBox1.Width - 10;
			pictureBox1.Location = new Point(x, y);
		}

		private double[][] LoadInputVector()
		{
			var count = 12;
			var inputVector = new double[count][];
			_tags = new List<string>(count);
			for (var i = 0; i < count; i++)
			{
				inputVector[i] = InputArrayFromFile("inputs2/" + i + ".txt");
				//Lab5.Vector.Normalize(inputVector[i], inputVector[i]);
				_inputValues.Add(i);
			}
			var tmpList = _inputValues.Distinct().ToList();
			_inputValues = tmpList;
			return inputVector;
		}

		private double[] InputArrayFromFile(string filePath)
		{
			var strings = File.ReadAllLines(filePath);
			var elements = new List<double>();
			_tags.Add(strings[0]);
			for (var i = 1; i < strings.Length; i++)
			{
				var row = strings[i].Split(' ');
				elements.AddRange(row.Select(elem => double.Parse(elem, CultureInfo.InvariantCulture)));
			}
			return elements.ToArray();
		}

		private void buttonLoadX_Click(object sender, EventArgs e)
		{
			var dialog = new OpenFileDialog();
			dialog.CheckFileExists = true;
			dialog.Multiselect = false;
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				var m = Lab2.BinaryMatrix.Load(dialog.FileName);
				if (m != null && m.GetLength(0) == matrixControl1.RowsCount && m.GetLength(1) == matrixControl1.ColsCount)
				{
					matrixControl1.SetMatrix(m);
				}
				else
				{
					MessageBox.Show("Wrong file format", "Error");
				}
			}
			tbResult.Text = "";
		}

		private void buttonSaveX_Click(object sender, EventArgs e)
		{
			var dialog = new SaveFileDialog();
			dialog.CheckPathExists = true;
			dialog.OverwritePrompt = true;
			dialog.DefaultExt = ".txt";
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				Lab2.BinaryMatrix.Save(dialog.FileName, matrixControl1.GetMatrix());
			}
		}

		private void buttonClear_Click(object sender, EventArgs e)
		{
			matrixControl1.ClearMatrix();
			tbResult.Text = "";
		}

		private void buttonRecognize_Click(object sender, EventArgs e)
		{
			if (_net == null)
			{
				return;
			}
			var inputs = matrixControl1.GetInputs();
			var temp = new double[matrixControl1.RowsCount * matrixControl1.ColsCount];
			for (int i = 0; i < temp.Length; i++)
			{
				temp[i] = inputs[i] == -1 ? 0 : 1;
			}
			//Lab5.Vector.Normalize(temp, temp);
			var val = _net.RecognizeValue(temp);
			System.Diagnostics.Debug.WriteLine("Value {0:f3}", val);
			var index = (int)Math.Round(val);
			tbResult.Text = index >= 0 && index < _tags.Count ? _tags[index] : "";
		}

		private void buttonTeach_Click(object sender, EventArgs e)
		{
			_net = new RbfNetwork(_inputVector, _inputValues, _inputVector.Length);
		}
	}
}
