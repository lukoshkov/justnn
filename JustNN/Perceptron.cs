﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNN
{
	public class Perceptron
	{
		private float[] _weights;
		private float c = 0.01f;
		private Func<float, float> _activationFunc;

		public float Bias { get; set; }

		public float[] Weights
		{
			get { return _weights; }
		}

		public Func<float, float> ActivationFunc
		{
			set
			{
				if (value != null)
				{
					_activationFunc = value;
				}
			}
		}

		public Perceptron(int inputsCount)
		{
			var random = new Random();
			_weights = new float[inputsCount];
			for (int i = 0; i < inputsCount; i++)
			{
				_weights[i] = (float)(random.NextDouble() * 2 - 1);
			}
			Bias = (float)(random.NextDouble() * 2 - 1);
			_activationFunc = x => x > 0 ? 1 : -1;
		}

		public float Activate(float[] inputs)
		{
			float s = Bias;
			for (int i = 0; i < _weights.Length; i++)
			{
				s += _weights[i] * inputs[i];
			}
			return _activationFunc(s);
		}

		public void Train(float[] inputs, int desired)
		{
			var guess = Activate(inputs);
			var error = desired - guess;
			for (int i = 0; i < _weights.Length; i++)
			{
				_weights[i] += c * error * inputs[i];
			}
			Bias += c * error;
		}
	}
}
