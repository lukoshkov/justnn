﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNN
{
	public class PointSet
	{
		public enum PointSetType
		{
			Points,
			Line
		}

		public List<PointF> Points { get; set; }
		public Pen Pen { get; set; }
		public PointSetType Type { get; set; }

		public PointSet(Pen pen, PointSetType type)
		{
			Points = new List<PointF>();
			Pen = pen;
			Type = type;
		}

		public void SetPointsByFunc(Func<float, float> f, float minX, float maxX, int count)
		{
			Points = new List<PointF>();
			var len = maxX - minX;
			var dx = len / (count - 1);
			for (int i = 0; i < count; i++)
			{
				var x = minX + i * dx;
				Points.Add(new PointF(x, f(x)));
			}
		}
	}
}
