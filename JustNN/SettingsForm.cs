﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JustNN
{
	public partial class SettingsForm : Form
	{
		private float sigma = 1;
		private float k = 1;
		private string[] funcNames = { "linear", "bipolar", "sigmoidal" };
		private Func<float, float>[] funcs;
		private PointSet[] sets;
		private Perceptron perceptron;
		private LearningSet learning;
		private Form1 form1;
		private System.Threading.Thread thread;
		private int iterations = 50;
		public event Action WeightsUpdated;

		public SettingsForm()
		{
			InitializeComponent();
			funcs = new Func<float, float>[]
			{
				x => k * x,
				x => x > 0 ? 1 : -1,
				x => x / (float)(Math.Abs(x) + sigma),
				x => 1 / (float)(1 + Math.Exp(-sigma * x))
			};
			sets = funcs.Select(f => SetFromFunc(f)).ToArray();
			coordinatePlane1.YSize = 2;
			perceptron = new Perceptron(2);
			UpdateWeights();
			comboBox1.Items.AddRange(funcNames);
			comboBox1.SelectedIndexChanged += ComboBox1_SelectedIndexChanged;
			comboBox1.SelectedIndex = 0;
			form1 = new Form1();
			learning = new LearningSet();
			learning.Init(form1.Plane.PointsA, form1.Plane.PointsB);
			form1.Plane.Sets.Add(DiscriminantLine(form1.Plane.XSize, form1.Plane.YSize));
			var formThread = new System.Threading.Thread(new System.Threading.ThreadStart(() => Application.Run(form1)));
			formThread.Start();
			WeightsUpdated += () => UpdateWeights();
			WeightsUpdated.Invoke();
			CheckForIllegalCrossThreadCalls = false;
		}

		public void StartLearning()
		{
			var plane = form1.Plane;
			for (int i = 0; i < iterations; i++)
			{
				for (int j = 0; j < learning.Points.Count; j++)
				{
					var p = learning.Points[j];
					perceptron.Train(new float[] { p.X, p.Y }, learning.Classes[j]);
				}
				WeightsUpdated.Invoke();
				plane.Sets[0] = DiscriminantLine(plane.XSize, plane.YSize);
				plane.Invalidate();
				System.Threading.Thread.Sleep(100);
			}
		}

		public PointSet DiscriminantLine(float xSize, float ySize)
		{
			var set = new PointSet(Pens.Green, PointSet.PointSetType.Line);
			if (perceptron.Weights[1] != 0)
			{
				var k = -perceptron.Weights[0] / perceptron.Weights[1];
				var b = -perceptron.Bias / perceptron.Weights[1];
				set.SetPointsByFunc(x => k * x + b, -xSize, xSize, 2);
			}
			else
			{
				set.Points.Add(new PointF(0, -ySize));
				set.Points.Add(new PointF(0, ySize));
			}
			return set;
		}

		private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			var i = comboBox1.SelectedIndex;
			if (i == 1)
			{
				label5.Visible = false;
				tbFactor.Visible = false;
			}
			else
			{
				label5.Visible = true;
				tbFactor.Visible = true;
				if (i == 0)
				{
					tbFactor.Text = k.ToString();
				}
				else
				{
					tbFactor.Text = sigma.ToString();
				}
			}
			perceptron.ActivationFunc = funcs[i];
			coordinatePlane1.Sets.Clear();
			coordinatePlane1.Sets.Add(sets[i]);
			coordinatePlane1.Invalidate();
		}

		private PointSet SetFromFunc(Func<float, float> f)
		{
			var set = new PointSet(Pens.Red, PointSet.PointSetType.Line);
			set.SetPointsByFunc(f, -coordinatePlane1.XSize, coordinatePlane1.XSize, 20);
			return set;
		}

		private void UpdateWeights()
		{
			tbWeight1.Text = perceptron.Weights[0].ToString();
			tbWeight2.Text = perceptron.Weights[1].ToString();
			tbBias.Text = perceptron.Bias.ToString();
		}

		private void textBox_KeyPress(object sender, KeyPressEventArgs e)
		{
			var tb = sender as TextBox;
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != ',' && e.KeyChar != '-')
			{
				e.Handled = true;
			}
			
			if (e.KeyChar == ',' && tb.Text.IndexOf(',') > -1)
			{
				e.Handled = true;
			}
			else if (e.KeyChar == '-' && tb.Text.Length > 0)
			{
				e.Handled = true;
			}
		}

		private void textBox_Validating(object sender, CancelEventArgs e)
		{
			var tb = sender as TextBox;
			float res;
			if (!float.TryParse(tb.Text, out res))
			{
				// Cancel the event and select the text to be corrected by the user.
				e.Cancel = true;
				tbWeight1.Select(0, tb.Text.Length);
			}
		}

		private void tbFactor_Validated(object sender, EventArgs e)
		{
			var i = comboBox1.SelectedIndex;
			var value = float.Parse(tbFactor.Text);
			if (i == 0)
			{
				k = value;
			}
			else if (i == 2)
			{
				sigma = value;
			}
			if (i != 1)
			{
				sets[i] = SetFromFunc(funcs[i]);
				perceptron.ActivationFunc = funcs[i];
				coordinatePlane1.Sets.Clear();
				coordinatePlane1.Sets.Add(sets[i]);
				coordinatePlane1.Invalidate();
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (thread != null && thread.IsAlive)
			{
				return;
			}
			//StartLearning();
			//return;
			var iter = (int)double.Parse(tbIterations.Text);
			iterations = iter > 0 ? iter : iterations;
			tbIterations.Text = iterations.ToString();
			thread = new System.Threading.Thread(new System.Threading.ThreadStart(() => StartLearning()));
			thread.Start();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			perceptron = new Perceptron(2);
			UpdateWeights();
			var plane = form1.Plane;
			plane.Sets[0] = DiscriminantLine(plane.XSize, plane.YSize);
			plane.Invalidate();
		}

		protected override void OnClosed(EventArgs e)
		{
			base.OnClosed(e);
			form1.Close();
			if (thread != null) thread.Abort();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			form1.StartPosition = FormStartPosition.Manual;
			form1.Location = new Point(Location.X + Width, Location.Y);
		}

		private void buttonLoad_Click(object sender, EventArgs e)
		{
			var dialog = new OpenFileDialog();
			dialog.CheckFileExists = true;
			dialog.Multiselect = false;
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				form1.LoadPoints(dialog.FileName);
				var lSet = new LearningSet();
				lSet.Init(form1.Plane.PointsA, form1.Plane.PointsB);
				learning = lSet;
			}
		}

		private void buttonSave_Click(object sender, EventArgs e)
		{
			var dialog = new SaveFileDialog();
			dialog.CheckPathExists = true;
			dialog.OverwritePrompt = true;
			dialog.DefaultExt = ".txt";
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				form1.SavePoints(dialog.FileName);
			}
		}

		private void buttonGenerate_Click(object sender, EventArgs e)
		{
			form1.GeneratePoints();
			var lSet = new LearningSet();
			lSet.Init(form1.Plane.PointsA, form1.Plane.PointsB);
			learning = lSet;
		}
	}
}
