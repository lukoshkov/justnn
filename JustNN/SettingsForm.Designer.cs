﻿namespace JustNN
{
	partial class SettingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.tbWeight1 = new System.Windows.Forms.TextBox();
			this.tbWeight2 = new System.Windows.Forms.TextBox();
			this.tbBias = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbFactor = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.coordinatePlane1 = new JustNN.CoordinatePlane();
			this.buttonStart = new System.Windows.Forms.Button();
			this.buttonReset = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.tbIterations = new System.Windows.Forms.TextBox();
			this.buttonSave = new System.Windows.Forms.Button();
			this.buttonLoad = new System.Windows.Forms.Button();
			this.buttonGenerate = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(271, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(47, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "weight 1";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(271, 42);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(47, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "weight 2";
			// 
			// tbWeight1
			// 
			this.tbWeight1.Location = new System.Drawing.Point(324, 13);
			this.tbWeight1.Name = "tbWeight1";
			this.tbWeight1.Size = new System.Drawing.Size(100, 20);
			this.tbWeight1.TabIndex = 2;
			this.tbWeight1.Text = "0";
			this.tbWeight1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
			this.tbWeight1.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
			// 
			// tbWeight2
			// 
			this.tbWeight2.Location = new System.Drawing.Point(324, 39);
			this.tbWeight2.Name = "tbWeight2";
			this.tbWeight2.Size = new System.Drawing.Size(100, 20);
			this.tbWeight2.TabIndex = 3;
			this.tbWeight2.Text = "0";
			this.tbWeight2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
			this.tbWeight2.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
			// 
			// tbBias
			// 
			this.tbBias.Location = new System.Drawing.Point(324, 65);
			this.tbBias.Name = "tbBias";
			this.tbBias.Size = new System.Drawing.Size(100, 20);
			this.tbBias.TabIndex = 5;
			this.tbBias.Text = "0";
			this.tbBias.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
			this.tbBias.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(292, 68);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(26, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "bias";
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Location = new System.Drawing.Point(95, 12);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(100, 21);
			this.comboBox1.TabIndex = 6;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(77, 13);
			this.label4.TabIndex = 8;
			this.label4.Text = "activation func";
			// 
			// tbFactor
			// 
			this.tbFactor.Location = new System.Drawing.Point(95, 39);
			this.tbFactor.Name = "tbFactor";
			this.tbFactor.Size = new System.Drawing.Size(100, 20);
			this.tbFactor.TabIndex = 9;
			this.tbFactor.Text = "1";
			this.tbFactor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
			this.tbFactor.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
			this.tbFactor.Validated += new System.EventHandler(this.tbFactor_Validated);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(55, 42);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(34, 13);
			this.label5.TabIndex = 10;
			this.label5.Text = "factor";
			// 
			// coordinatePlane1
			// 
			this.coordinatePlane1.GridVisible = true;
			this.coordinatePlane1.Location = new System.Drawing.Point(15, 68);
			this.coordinatePlane1.Name = "coordinatePlane1";
			this.coordinatePlane1.PointsA = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("coordinatePlane1.PointsA")));
			this.coordinatePlane1.PointsB = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("coordinatePlane1.PointsB")));
			this.coordinatePlane1.Size = new System.Drawing.Size(250, 250);
			this.coordinatePlane1.TabIndex = 7;
			this.coordinatePlane1.TagsVisible = false;
			this.coordinatePlane1.XSize = 5F;
			this.coordinatePlane1.YSize = 5F;
			// 
			// buttonStart
			// 
			this.buttonStart.Location = new System.Drawing.Point(349, 181);
			this.buttonStart.Name = "buttonStart";
			this.buttonStart.Size = new System.Drawing.Size(75, 23);
			this.buttonStart.TabIndex = 11;
			this.buttonStart.Text = "Start";
			this.buttonStart.UseVisualStyleBackColor = true;
			this.buttonStart.Click += new System.EventHandler(this.button1_Click);
			// 
			// buttonReset
			// 
			this.buttonReset.Location = new System.Drawing.Point(349, 151);
			this.buttonReset.Name = "buttonReset";
			this.buttonReset.Size = new System.Drawing.Size(75, 23);
			this.buttonReset.TabIndex = 12;
			this.buttonReset.Text = "Reset";
			this.buttonReset.UseVisualStyleBackColor = true;
			this.buttonReset.Click += new System.EventHandler(this.button2_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(270, 119);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(49, 13);
			this.label6.TabIndex = 13;
			this.label6.Text = "iterations";
			// 
			// tbIterations
			// 
			this.tbIterations.Location = new System.Drawing.Point(325, 116);
			this.tbIterations.Name = "tbIterations";
			this.tbIterations.Size = new System.Drawing.Size(99, 20);
			this.tbIterations.TabIndex = 14;
			this.tbIterations.Text = "50";
			this.tbIterations.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
			this.tbIterations.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
			// 
			// buttonSave
			// 
			this.buttonSave.Location = new System.Drawing.Point(349, 258);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(75, 23);
			this.buttonSave.TabIndex = 15;
			this.buttonSave.Text = "Save points";
			this.buttonSave.UseVisualStyleBackColor = true;
			this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
			// 
			// buttonLoad
			// 
			this.buttonLoad.Location = new System.Drawing.Point(349, 287);
			this.buttonLoad.Name = "buttonLoad";
			this.buttonLoad.Size = new System.Drawing.Size(75, 23);
			this.buttonLoad.TabIndex = 16;
			this.buttonLoad.Text = "Load points";
			this.buttonLoad.UseVisualStyleBackColor = true;
			this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
			// 
			// buttonGenerate
			// 
			this.buttonGenerate.Location = new System.Drawing.Point(349, 229);
			this.buttonGenerate.Name = "buttonGenerate";
			this.buttonGenerate.Size = new System.Drawing.Size(75, 23);
			this.buttonGenerate.TabIndex = 17;
			this.buttonGenerate.Text = "Generate";
			this.buttonGenerate.UseVisualStyleBackColor = true;
			this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
			// 
			// SettingsForm
			// 
			this.ClientSize = new System.Drawing.Size(442, 328);
			this.Controls.Add(this.buttonGenerate);
			this.Controls.Add(this.buttonLoad);
			this.Controls.Add(this.buttonSave);
			this.Controls.Add(this.tbIterations);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.buttonReset);
			this.Controls.Add(this.buttonStart);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.tbFactor);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.coordinatePlane1);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.tbBias);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.tbWeight2);
			this.Controls.Add(this.tbWeight1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "SettingsForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbWeight1;
		private System.Windows.Forms.TextBox tbWeight2;
		private System.Windows.Forms.TextBox tbBias;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox comboBox1;
		private CoordinatePlane coordinatePlane1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tbFactor;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button buttonStart;
		private System.Windows.Forms.Button buttonReset;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox tbIterations;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonLoad;
		private System.Windows.Forms.Button buttonGenerate;
	}
}