﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataSetGenerator
{
	class Program
	{
		static void GenerateSet(Random random, string filename, int setSize, Func<double, double, double> f, double minX, double maxX, double minY, double maxY)
		{
			using (var file = new System.IO.StreamWriter(filename))
			{
				var culture = System.Globalization.CultureInfo.InvariantCulture;
				file.WriteLine("x\ty\tz");
				for (int i = 0; i < setSize; i++)
				{
					var x = NextDoubleInRange(random, minX, maxX);
					var y = NextDoubleInRange(random, minY, maxY);
					var z = f(x, y);
					file.WriteLine("{0}\t{1}\t{2}", x.ToString(culture), y.ToString(culture), z.ToString(culture));
				}
			}
		}

		static double NextDoubleInRange(Random random, double min, double max)
		{
			return random.NextDouble() * (max - min) + min;
		}

		static void Main(string[] args)
		{
			var indicator = new Pos7Indicator('#', 4);
			var spasing = indicator.SegmentSize + 4;
			for (uint i = 0; i < 10; i++)
			{
				indicator.SetInputs(i);
				indicator.Print((int)(1 + i * spasing), 1);
			}

			var num = new Random().Next() % 100000;
			Pos7Indicator.PrintNumber(num, 1, indicator.SegmentSize * 2 + 6, '#', 4);
			Console.WriteLine("   {0}", num);
			Console.ReadLine();
			return;

			Func<double, double, double> f = (x, y) => Math.Abs(x + y) - x * x * x * y + 6 * y * y;
			var random = new Random();
			int setSize = 5000;
			GenerateSet(random, "training.txt", setSize, f, -2, 2, -2, 2);
			GenerateSet(random, "test.txt", setSize / 10, f, -2, 2, -2, 2);
		}
	}
}
