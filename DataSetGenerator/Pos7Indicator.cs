﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataSetGenerator
{
	public class Pos7Indicator
	{
		private int _segmentSize;

		public bool[] Inputs { get; private set; }
		public bool[] States { get; private set; }
		public char Symbol { get; set; }

		public int SegmentSize
		{
			get { return _segmentSize; }
			set
			{
				if (value > 0)
				{
					_segmentSize = value;
				}
			}
		}

		public Pos7Indicator(char symbol = '#', int segmentSize = 3)
		{
			Inputs = new bool[4];
			States = new bool[7];
			_segmentSize = 3;
			SegmentSize = segmentSize;
			Symbol = symbol;
		}

		public void SetInputs(uint value)
		{
			Inputs[0] = (value & 8) > 0;
			Inputs[1] = (value & 4) > 0;
			Inputs[2] = (value & 2) > 0;
			Inputs[3] = (value & 1) > 0;
			UpdateState();
		}

		public void UpdateState()
		{
			States[0] = !Inputs[0] && Inputs[2]
				|| !Inputs[0] && Inputs[1] && Inputs[3]
				|| Inputs[0] && !Inputs[1] && !Inputs[2]
				|| !Inputs[1] && !Inputs[2] && !Inputs[3];

			States[1] = !Inputs[1] && !Inputs[2] && !Inputs[3]
				|| !Inputs[0] && Inputs[1] && !Inputs[2]
				|| !Inputs[0] && Inputs[1] && !Inputs[3]
				|| Inputs[0] && !Inputs[1] && !Inputs[2];

			States[2] = !Inputs[0] && !Inputs[1]
				|| !Inputs[1] && !Inputs[2]
				|| !Inputs[0] && !Inputs[2] && !Inputs[3]
				|| !Inputs[0] && Inputs[2] && Inputs[3];

			States[3] = !Inputs[0] && Inputs[1] && !Inputs[2]
				|| Inputs[0] && !Inputs[1] && !Inputs[2]
				|| !Inputs[0] && !Inputs[1] && Inputs[2]
				|| !Inputs[0] && Inputs[2] && !Inputs[3];

			States[4] = !Inputs[1] && !Inputs[2] && !Inputs[3]
				|| !Inputs[0] && Inputs[2] && !Inputs[3];

			States[5] = !Inputs[0] && Inputs[1]
				|| !Inputs[0] && Inputs[3]
				|| !Inputs[1] && !Inputs[2];

			States[6] = !Inputs[0] && !Inputs[1] && Inputs[2]
				|| !Inputs[0] && Inputs[2] && !Inputs[3]
				|| !Inputs[1] && !Inputs[2] && !Inputs[3]
				|| Inputs[0] && !Inputs[1] && !Inputs[2]
				|| !Inputs[0] && Inputs[1] && !Inputs[2] && Inputs[3];
		}

		public void Print(int x, int y)
		{
			var count = SegmentSize;
			if (States[0]) PrintHorizontal(Symbol, x + 1, y, count);
			y++;
			if (States[1]) PrintVertical(Symbol, x, y, count);
			if (States[2]) PrintVertical(Symbol, x + 1 + count, y, count);
			y += count;
			if (States[3]) PrintHorizontal(Symbol, x + 1, y, count);
			y++;
			if (States[4]) PrintVertical(Symbol, x, y, count);
			if (States[5]) PrintVertical(Symbol, x + 1 + count, y, count);
			y += count;
			if (States[6]) PrintHorizontal(Symbol, x + 1, y, count);
		}

		public static void PrintNumber(int number, int x, int y, char symbol, int segmentSize)
		{
			var num = (uint)number;
			var stack = new Stack<uint>();
			var indicator = new Pos7Indicator(symbol, segmentSize);
			var spacing = indicator.SegmentSize + 4;
			while (num != 0)
			{
				stack.Push(num % 10);
				num /= 10;
			}
			var i = 0;
			while (stack.Count > 0)
			{
				indicator.SetInputs(stack.Pop());
				indicator.Print(x + i * spacing, y);
				i++;
			}
		}

		private static void PrintHorizontal(char symbol, int x, int y, int count)
		{
			Console.SetCursorPosition(x, y);
			for (int i = 0; i < count; i++)
			{
				Console.Write(symbol);
			}
		}

		private static void PrintVertical(char symbol, int x, int y, int count)
		{
			for (int i = 0; i < count; i++)
			{
				Console.SetCursorPosition(x, y + i);
				Console.Write(symbol);
			}
		}
	}
}
